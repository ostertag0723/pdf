import React, { useEffect, useRef } from "react";
import { Place } from "../../components/Place";
import cross from "../../assets/cross.svg";

import { EnhancedEncodeParam, qrEncode } from "../../utils/qr";
import styled from "styled-components";
import QrCodeWithLogo from "qrcode-with-logos";
import baseIcon from "../../assets/baseIcon.png";
import bgimg from "../../assets/baseIcon2.png";

type Props = {
  data: EnhancedEncodeParam;
  onLeave: () => void;
};

export const QRPreview = ({ data, onLeave }: Props) => {
  const imgRef = useRef<HTMLImageElement>(null);
  const fullString = data.part1 + data.part2 + data.part3 + data.part4;

  useEffect(() => {
    if (!imgRef.current) return;

    const encodedString = qrEncode(data);

    new QrCodeWithLogo({
      image: imgRef.current,
      content: encodedString,
      width: 380,
      logo: {
        src: data.customImg || baseIcon,
        logoRadius: 8,
        borderSize: 0,
      },
    }).toImage();
  }, [data]);

  return data ? (    
    <PageWrapper>
      <Header>
        <Cross src={cross} onClick={onLeave} />
      </Header>
      <MessageWrapper>
        <PlaceWrapper>
            <StyledPlace value={fullString || ""} readOnly />          
        </PlaceWrapper>
      </MessageWrapper>
      <TickWrapper>
        <TickWrapperInner>
          <StyledQrCode ref={imgRef} alt="qrCode" />
        </TickWrapperInner>
      </TickWrapper>
      <Address>
          <div>{data.part1}</div>
          <div>{data.part2}</div>
          <div>{data.part3}</div>
          <div>{data.part4}</div>
      </Address>
      
      <div style={{ backgroundImage: `url(${bgimg})` }}></div>
      

    </PageWrapper>
  ) : (
    <></>
  );
};


const PageWrapper = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  flex-direction: column;
  color: #000;
  background-color: #fff;
  position: absolute;
`;

const Header = styled.div`
  width: 100%;
  display: flex;
  justify-content: flex-end;
  flex-shrink: 0;
`;

const Cross = styled.img`
  height: 20px;
  margin: 24px;
`;

const StyledPlace = styled(Place)`
  font-size: 20px;
  line-height: 20px;
  text-shadow: none;
`;

const PlaceWrapper = styled.div`
  padding: 0 32px;
`;

const MessageWrapper = styled.div`
  width: 100%;
  height: 50%;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const TickWrapper = styled.div`
  height: 100%;
  width: 100%;
  text-align: center;
`;

const TickWrapperInner = styled.div`
  height: 100%;
  max-height: 280px;
  display: flex;
  flex-direction: column;
  justify-content: center;
`;

const StyledQrCode = styled.img`
  display: inline-block;
  height: 100%;
  max-height: 300px;
  margin: 0 auto;
`;

const Address = styled.div`
  width: 100%;
  height: 50%;
  text-align: center;
`;
