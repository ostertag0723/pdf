import {
  Button,
  ButtonGroup,
  Divider,
  FormGroup,
  TextField,
} from "@material-ui/core";
import React, { useEffect, useRef, useState } from "react";
import { useSetState, useMount } from "react-use";
import styled from "styled-components";
import { Header } from "../../components/Header";
import { EnhancedEncodeParam, qrEncode } from "../../utils/qr";
import QrCodeWithLogo from "qrcode-with-logos";
import baseIcon from "../../assets/baseIcon.png";

import { disableBodyScroll } from "body-scroll-lock";
import SaveIcon from "@material-ui/icons/Save";
import { QRPreview } from "./QRPreview";

export const QRGenerator = () => {
  const imgRef = useRef<HTMLImageElement>(null);
  //const fileFieldRef = React.useRef<HTMLInputElement>(null);

  const [showPreview, setShowPreview] = useState(false);
  const [qrCode, setQrCode] = useState<QrCodeWithLogo | null>(null);
  const [state, setState] = useSetState<EnhancedEncodeParam>({
    part1: "m",
	part2: "b7e9a",
	part3: "61161",
	part4: "8320374",
    customImg: null,
  });

  const isVenueCodeValid = 1;
  const isVenueIdValid = 8;

  const isValidData = isVenueCodeValid && isVenueIdValid;

  useMount(() => {
    const root = document.querySelector("#scroll");
    if (!root) return;
    disableBodyScroll(root);
  });

  useEffect(() => {
    if (!imgRef.current || !isValidData) return;
    const encodedString = qrEncode(state);

    const qrCode = new QrCodeWithLogo({
      image: imgRef.current,
      content: encodedString,
      width: 380,
      logo: {
        src: state.customImg || baseIcon,
        logoRadius: 8,
        borderSize: 0,
      },
    });

    qrCode.toImage();
    setQrCode(qrCode);
  }, [state, isValidData]);

 

  const handleDownload = () => {
    if (!qrCode) return;
    qrCode.downloadImage("QR Code");
  };

  return (
    <PageWrapper>
      <Header backPath="/" name="生成二維碼 - s 66c6b 61381 2595443" />
      <ContentWrapper id="scroll">
        <StyledForm>          
          <StyledTextField
            label="Single / Month 1-bit (part1)"
            value={state.part1}
            onChange={(e) => {
              setState({ part1: e.target.value });
            }}
          />
		  <StyledTextField
            label="Hex hash 5-bits (part2)"
            value={state.part2}
            onChange={(e) => {
              setState({ part2: e.target.value });
            }}
          />
		  <StyledTextField
            label="Month 5-bits (part3)"
            value={state.part3}
            onChange={(e) => {
              setState({ part3: e.target.value });
            }}
          />
		  <StyledTextField
            label="Sequence No. 7-bits (part4)"
            value={state.part4}
            onChange={(e) => {
              setState({ part4: e.target.value });
            }}
          />
          
    
        </StyledForm>
        <Divider />
        <Actions>
          <ButtonGroup aria-label="outlined primary button group">
            <Button
              variant="contained"
              size="small"
              startIcon={<SaveIcon />}
              onClick={handleDownload}
              disabled={!isValidData}
            >
              儲存
            </Button>
            <Button
              variant="contained"
              size="small"
              startIcon={<SaveIcon />}
              onClick={() => {
                setShowPreview(true);
              }}
              disabled={!isValidData}
            >
              預覽
            </Button>
          </ButtonGroup>
        </Actions>
        <StyledQrCode ref={imgRef} alt="qrCode" />
      </ContentWrapper>
      {showPreview && (
        <QRPreview
          data={state}
          onLeave={() => {
            setShowPreview(false);
          }}
        />
      )}
    </PageWrapper>
  );
};

const PageWrapper = styled.div`
  width: 100%;
  height: 100%;
  background-color: #fff;
  display: flex;
  flex-direction: column;
`;

const ContentWrapper = styled.div`
  width: 100%;
  height: 100%;
  overflow: auto;
`;

const StyledForm = styled(FormGroup)`
  padding: 8px 16px;
`;

const StyledTextField = styled(TextField)`
  &.MuiFormControl-root {
    margin-top: 8px;
  }
`;



const StyledQrCode = styled.img`
  width: 100%;
`;

const Actions = styled.div`
  margin-top: 16px;
  display: flex;
  justify-content: center;
`;
